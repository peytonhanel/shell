# Change Log

<h2>Shell v3</h2>
<h4>2019-02-25</h4>

**Added**
- "echo" command. Prints parameters back to screen.
    - param -n removes trailing newline character.
    - ONLY using param $? prints the internal exit status of the last command
      ran.
- Processes can now be run in the background by making the very last parameter
  an ampersand (i.e. "&").

**Bugs**
- Tester only works on Windows (not sure about Mac).
- [FIXED] Commands without parameters not working on Macs.

<h2>Shell v2</h2>
<h4>2019-02-17</h4>

**Added**
- error messages on incorrect input
- "cd" command, changes directory
    - supports relative paths, absolute paths
    - use param ".." to drop down a level in the hierarchy
- "pwd" command, prints current working directory
- can run any installed OS process
- can execute programs that are in the current working directory
- prompt now displays working directory

**Removed**
- printing input to the screen with every command