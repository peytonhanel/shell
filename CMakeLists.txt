cmake_minimum_required(VERSION 3.7.2)
PROJECT(Shell)

set(CMAKE_C_STANDARD 11)

add_executable(Shell shell_cmd.c shell_cmd.h shell.c shell.h)