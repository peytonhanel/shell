# The Chitin Shell

Welcome to The Chitin Shell! This is a simple shell program assigned in my
Operating Systems course, [CSIS 460](https://bsnider.cs.georgefox.edu/courses/csis460-operating-systems).
It has so far taught me:<br>
- shell basics<br>
- creating new processes (i.e.: forking and execvp)<br>
- syscalls<br>
- background jobs<br>
- shell testing<br>
- and more!<br>

### Supported Commands

- echo
- pwd
- cd
- ls
- exit
- can create new processes, as well as in the background

# Change Log

<h2>Shell v3</h2>
<h4>2019-02-25</h4>

**Added**
- "echo" command. Prints parameters back to screen.
    - param -n removes trailing newline character.
    - ONLY using param $? prints the internal exit status of the last command
      ran.
- Processes can now be run in the background by making the very last parameter
  an ampersand (i.e. "&").

**Bugs**
- Tester only works on Windows (not sure about Mac).
- [FIXED] Commands without parameters not working on Macs.