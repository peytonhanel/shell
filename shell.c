/**
 * @file shell.c
 * @brief A command-line shell program.
 *
 * A command-line shell program that uses system calls to directly invoke
 * operating system services, rather than relying on C library calls.
 *
 * @author Peyton Hanel (phanel16@georgefox.edu)
 */

#include "shell.h"


int main()
{
   char input[MAX_COMMAND_LENGTH];
   int numArgs;
   int success;

   // the main shell program
   while (1)
   {
      // gets input from user, each token separated by spaces gets put into an
      // array, then executes the input
      promptUserForInput(input);
      numArgs = getNumArgs(input);
      char* args[numArgs + 1]; // +1 for last element being NULL
      tokenizeInput(args, input, numArgs);

      // execute commands
      success = execute(args, numArgs);
   }
   return 0;
}

void promptUserForInput(char* input)
{
   char currentDir[MAX_DIR_LENGTH];
   size_t i = 0;
   size_t currentDirLength;

   // gets current working directory, appends PROMPT to the end of it
   getcwd(currentDir, MAX_DIR_LENGTH);

   // finds the end of the string
   while (currentDir[i] != 0) {
      i++;
   }
   currentDir[i] = ' ';
   currentDir[i + 1] = PROMPT;
   currentDir[i + 2] = 0;
   currentDirLength = i + 2;

   // display the prompt
   write(STDOUT_FILENO, currentDir, currentDirLength);

   // initialize input with nulls to clear it from whatever was there before
   for (int j = 0; j < MAX_COMMAND_LENGTH; j++) {
      input[j] = 0;
   }
   // reads the input
   read(STDIN_FILENO, input, MAX_COMMAND_LENGTH);
}

int getNumArgs(char* input)
{
   int numArgs = 0;
   char previousChar = ' ';
   bool done = false;

   // counts the number of arguments in input. Arguments separated by spaces
   for (int i = 0;
        i < MAX_COMMAND_LENGTH && !done;
        i++)
   {
      // reached the end of an argument, increment count
      if (input[i] == ' ' && previousChar != ' ') {
         numArgs++;
      }
      // at end of input, done
      if (input[i] == '\n' || input[i] == 0) {
         numArgs++;
         done = true;
      }
      previousChar = input[i];
   }
   return numArgs;
}

void tokenizeInput(char** args, char* input, int numArgs)
{
   // todo: allow for quotations to make arguments that contain spaces

   // multiple calls to strtok puts each arg token from input into args
   args[0] = strtok(input, " ");
   for (int i = 1; i <= numArgs; i++) {
      args[i] = strtok(NULL, " ");
   }
   // remove new line from last arg
   killNewLine(args, numArgs);
}

void killNewLine(char** args, int numArgs)
{
   // purges new line char from last arg
   char *newline = strchr(args[numArgs - 1], '\n');
   if (newline) {
      *newline = 0;
   }
}

int execute(char** args, int numArgs)
{
   static int success;

   // prints current working directory
   if (compareCommands(args[0], CMD_PRINT_DIR)) {
      success = shell_cmd_pwd();
   }
   // changes working directory
   else if (compareCommands(args[0], CMD_CHANGE_DIR)) {
      success = shell_cmd_cd(args, numArgs);
   }
   // exit shell
   else if (compareCommands(args[0], CMD_EXIT)) {
      success = shell_cmd_exit();
   }
   // echo (prints back parameters to screen)
   else if (compareCommands(args[0], CMD_ECHO)) {
      success = shell_smd_echo(args, numArgs, success);
   }
   // all other commands, fork and try to execute a new process
   else {
      success = createNewProcess(args, numArgs);
   }
   return success;
}

int createNewProcess(char** args, int numArgs)
{
   int success = 0;
   bool runInBackground = false;

   // Check if user wants this command to run in the background
   if (strcmp(args[numArgs - 1], ARG_BKGND) == 0) {
      runInBackground = true;
      args[numArgs - 1] = NULL;
   }

   pid_t fork_pid = fork();
   pid_t wait_pid;
   int wait_status;

   // checks if user wants to run in background

   // child process
   if (fork_pid == 0) {
      success = execvp(args[0], args); // DOESN'T RETURN

      // upon making it to this point, execvp has failed and this process
      // must terminate. Prints an error message
      perror("command failed");
      _exit(errno);
   }
   // parent process
   else if (fork_pid > 0) {

      if (!runInBackground) {
         // waits for child processes to close
         wait_pid = wait(&wait_status);
         if (wait_pid < 0) {
            perror("wait for command failed");
            success = errno;
         }
      }
      else {
         // todo: print visual
      }
   }
   // error
   else {
      perror("fork failed");
      success = errno;
   }
   return success;
}
