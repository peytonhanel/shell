/**
 * @file shell.h
 * @brief Function prototypes for the shell.
 *
 * @author Peyton Hanel (phanel16@georgefox.edu)
 */

#include <sys/syscall.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "shell_cmd.h"

#ifndef SHELL_H
#define SHELL_H


/**
 * @brief Main entry point of the shell.
 *
 * @return The exit status of the shell.
 */
int main();

/**
 * Executes the commands and parameters found in args.
 *
 * @param args The arguments from user input, the first being an actual command,
 *             the following elements being parameters for that command, and the
 *             last element being NULL.
 * @param numArgs Excluding the NULL at the end, The number of elements in args.
 * @return True if the shell is to quit running.
 */
int execute(char** args, int numArgs);

/**
 * Copies each token separated by spaces from input and puts each into args as
 * its own array element.
 *
 * @param input Contains the original user input.
 * @param args An empty char** that will be filled with elements from input. The
 *             first element will be the actual command, the following elements
 *             will be parameters for that command, and the last element will
 *             be NULL.
 * @param numArgs The number of space separated tokens in input. Also the number
 *                of elements that will be in args, excluding the null at the
 *                end.
 */
void tokenizeInput(char** args, char* input, int numArgs);

/**
 * Prints a prompt to the shell and waits for the user to enter input. Will
 * fill the input parameter with the user input.
 *
 * @param input To be filled with the user input.
 */
void promptUserForInput(char* input);

/**
 * Attempts to spawn a new process from the command and parameters
 * found in args.
 *
 * @param args The arguments from user input, the first being an actual command,
 *             the following elements being parameters for that command, and the
 *             last element being NULL.
 * @return True if the shell is to quit running.
 */
int createNewProcess(char** args, int numArgs);

/**
 * Gets the number of tokens separated by spaces from input.
 *
 * @param input Contains the original user input.
 * @return The number of tokens separated by spaces from input.
 */
int getNumArgs(char* input);

/**
 * Removes a new line character from the end of the last element in args, if
 * there is one.
 *
 * @param args The arguments from user input, the first being an actual command,
 *             the following elements being parameters for that command, and the
 *             last element being NULL.
 * @param numArgs Excluding the NULL at the end, The number of elements in args.
 */
void killNewLine(char** args, int numArgs);


#endif /* SHELL_H */
