/**
 * @file shell_cmd.c
 * @brief Functions for individual commands implemented by the Chitin shell.
 *
 * A library of all the methods needed to invoke specific commands and their
 * parameters.
 *
 * @author Peyton Hanel (phanel16@georgefox.edu)
 */

#include "shell_cmd.h"


int shell_cmd_pwd()
{
   int success = 0;

   // gets the current working directory and prints it
   char dir[MAX_DIR_LENGTH];
   if (getcwd(dir, MAX_DIR_LENGTH) != NULL) {
      write(STDOUT_FILENO, dir, strlen(dir));
      write(STDOUT_FILENO, "\n", 1);
   }
   else {
      success = 1;
   }
   return success;
}

int shell_cmd_cd(char** args, int numArgs)
{
   int success = 0;

   // if error, print error message
   if (numArgs > 1 && chdir(args[1]) != 0)
   {
      // concatenate args into an error message and print it
      size_t errorMsgLength
            = strlen(args[0]) + strlen(args[1])+ strlen(COLON);
      char errorMsg[errorMsgLength];
      initializeString(errorMsg, errorMsgLength);

      // concatenate and print
      strcat(errorMsg, args[0]);
      strcat(errorMsg, COLON);
      strcat(errorMsg, args[1]);
      perror(errorMsg);

      success = errno;
   }
      // No parameters, tell the user to provide parameters
   else if (numArgs == 1)
   {
      // find length of array and create it
      size_t errorMsgLength
            = strlen(args[0]) + strlen(COLON) + strlen(ERMSG_NO_PARAMS) + 1;
      char errorMsg[errorMsgLength];
      initializeString(errorMsg, errorMsgLength);

      // concats each string together for the error message and prints it
      strcat(errorMsg, args[0]);
      strcat(errorMsg, COLON);
      strcat(errorMsg, ERMSG_NO_PARAMS);
      strcat(errorMsg, "\n");
      write(STDERR_FILENO, errorMsg, errorMsgLength);

      success = errno;
   }
   return success;
}

int shell_cmd_exit()
{
   // todo: returning number is meaningless but spec says to do it
   _exit(0);
   return 0;
}

int shell_smd_echo(char** args, int numArgs, int currentSuccess)
{
   int success = 0; // todo: set to 1 if error
   char* arg_n = "-n";
   char* arg_status = "$?";

   // no arguments so just print newline
   if (args[1] == NULL) {
      write(STDOUT_FILENO, "\n", 1);
   }
   // exclude trailing newline
   else if (strcmp(args[1], arg_n) == 0) {
      if (args[2] != NULL) {
         write(STDOUT_FILENO, args[2], strlen(args[2]));
      }
   }
   // exposes the currentSuccess variable to the user
   else if (strcmp(args[1], arg_status) == 0) {
      char successAsString[2];
      sprintf(successAsString, "%d", currentSuccess);
      write(STDOUT_FILENO, successAsString , strlen(successAsString));
      write(STDOUT_FILENO, "\n", 1);
   }
   // print provided argument
   else {
      write(STDOUT_FILENO, args[1], strlen(args[1]));
      write(STDOUT_FILENO, "\n", 1);
   }
   return success;
}

void initializeString(char* array, size_t length)
{
   // fill the array with 0s
   for (int i = 0; i < length; i++)
   {
      array[i] = 0;
   }
}

bool compareCommands(const char* first, const char* second)
{
   // compares the two strings, char by char.
   // stops at the new line or null character
   int i = 0;
   bool areEqual = true;
   bool done = false;
   while(!done && areEqual && i < MAX_COMMAND_LENGTH)
   {
      // checks if we are at end of either
      if ( first[i]  == '\n' || first[i]  == 0
           || second[i] == '\n' || second[i] == 0)
      {
         // is one of these shorter than the other?
         done = true;
         if (first[i] != second[i]) {
            areEqual = false;
         }
      }
      // are these the same so far?
      if (first[i] != second[i]) {
         areEqual = false;
      }
      i++;
   }
   // stops function from returning true if any of the parameters start with an
   // end character
   if (areEqual
       && (first[0]  == '\n' || first[0]  == 0
           ||  second[0] == '\n' || second[0] == 0))
   {
      areEqual = false;
   }
   return areEqual;
}
