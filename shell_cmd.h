/**
 * @file shell_cmd.h
 * @brief Function prototypes and constants for shell commands.
 * @author Peyton Hanel (phanel16@georgefox.edu)
 */

#include <unistd.h>
#include <memory.h>
#include <errno.h>
#include <stdio.h>
#include <stdbool.h>


#define MAX_COMMAND_LENGTH (255)
#define MAX_DIR_LENGTH (1000)

#ifndef SHELL_SHELL_CMD_H
#define SHELL_SHELL_CMD_H

static const char* COLON = ": ";
static const char PROMPT = '$';

static const char* CMD_EXIT = "exit";
static const char* CMD_PRINT_DIR = "pwd";
static const char* CMD_CHANGE_DIR = "cd";
static const char* CMD_ECHO = "echo";

static const char* ARG_BKGND = "&";

static const char* ERMSG_NO_FIND = "cannot find ";
static const char* ERMSG_NO_PARAMS = "please provide parameters";


/**
 * Prints the current working directory to stdout.
 *
 * @return Zero if working directory printed successfully, 1 if the directory
 * could not be determined.
 */
int shell_cmd_pwd();

/**
 * Changes the current working directory.
 *
 * @param args The arguments from user input, the first being an actual command,
 *             the following elements being parameters for that command, and the
 *             last element being NULL.
 * @param numArgs Excluding the NULL at the end, The number of elements in args.
 * @return Zero if changed directory successfully, non-zero if error.
 */
int shell_cmd_cd(char** args, int numArgs);

/**
 * Terminates the running shell.
 * @return Zero if changed directory successfully, non-zero if error.
 */
int shell_cmd_exit();

/**
 *  Prints the parameters back to stdout. Prints a new line unless the -n is
 *  used (-n is never printed). Invoking "echo $?" will print the last exit
 *  status.
 *
 * @param args The arguments from user input, the first being an actual command,
 *             the following elements being parameters for that command, and the
 *             last element being NULL.
 * @param numArgs Excluding the NULL at the end, The number of elements in args.
 * @return Zero if changed directory successfully, non-zero if error.
 */
int shell_smd_echo(char** args, int numArgs, int currentSuccess);

/**
 * Sets each element in array to be 0. Clears whatever crap was there before
 *
 * @param array The string to initialize.
 * @param length The length of array.
 */
void initializeString(char* array, size_t length);

/**
 * Compares two strings together, both of which are commands. Compares
 * character by character, until an either a new line character or an ascii
 * null is reached.
 *
 * @param first The first command.
 * @param second The second command.
 *
 * @return True if both commands, before the end characters, are the same.
 */
bool compareCommands(const char* first, const char* second);

#endif //SHELL_SHELL_CMD